app.controller('ErrorCtrl', [
    '$scope',
    'localStorageService',
    '$state',
    '$timeout',
    'utils',
    'fsm',
    'CONFIG',
    function ($scope, localStorageService, $state, $timeout, utils, fsm, CONFIG) {

        $scope.btn_aspecto = CONFIG.pantallas[CONFIG.pantalla].btn_aspecto;
        
        var codigo = localStorageService.get('codigo');

        fsm.get_machine(codigo).then(function (machine) {

            $scope.text = {
                titulo_screen_error: utils._('TITULO_SCREEN_ERROR_' + machine.state),
                finalizar: utils._('FINALIZAR')
            };

            var t = $timeout(function () {
                machine.timeout();
                localStorageService.set('state', machine.state);
                $state.go(machine.state, {}, { reload: true });
            }, 5000);

            $scope.finalizar = function () {
                $timeout.cancel(t);

                machine.abort();
                localStorageService.set('state', machine.state);
                $state.go(machine.state, {}, { reload: true });

            }
        });
    }]);