app.controller('DocCtrl', [
    '$scope',
    'localStorageService',
    '$state',
    'Notification',
    'utils',
    'fsm',
    'CONFIG',
    function ($scope, localStorageService, $state, Notification, utils, fsm, CONFIG) {
        
        $scope.css_keyboard = './styles/jkeyboard.' + CONFIG.pantalla + '.css';
        $scope.input_aspecto = CONFIG.pantallas[CONFIG.pantalla].input_aspecto;
        
        $scope.text = {            
            'intro_doc': utils._('INTRO_DOC'),            
        };

        $('#doc').focus();
                
        $('#keyboard').jkeyboard({
            layout: "alpha_p360",
            input: $('#doc'),
        });

        // Cuando se pulsa la tecla 'OK'
        $('.return').click(function(){
            var doc = $('#doc').val().toUpperCase();
            var codigo = localStorageService.get('codigo');
            fsm.get_machine(codigo).then(function (machine) {
                machine.sign(doc);
                localStorageService.set('state', machine.state);
                if (machine.state === "doc" && parseInt(localStorageService.get('tries_doc')) > 0) {
                    Notification.error(utils._('BAD_DOC') + ' ' + localStorageService.get('tries_doc'));
                } else {
                    $state.go(machine.state, {}, { reload: true });
                }
            });
        });
        
    }]);