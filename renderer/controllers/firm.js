const SignaturePad = require('signature_pad')

app.controller('FirmCtrl', [
    '$scope',
    'localStorageService',
    '$state',
    'Notification',
    'utils',
    'fsm',
    'CONFIG',
    function ($scope, localStorageService, $state, Notification, utils, fsm, CONFIG) {

        $scope.firma_canvas_width = CONFIG.pantallas[CONFIG.pantalla].firma_canvas_width;
        $scope.firma_canvas_height = CONFIG.pantallas[CONFIG.pantalla].firma_canvas_height;

        var canvas_firm = document.querySelector("canvas");

        // https://github.com/szimek/signature_pad
        var signaturePad = new SignaturePad(canvas_firm, {
            minWidth: 1,
            maxWidth: 3,
            penColor: "rgb(66, 133, 244)"
        });

        var isCanvasBlank = function (canvas) {
            var blank = document.createElement('canvas');
            blank.width = canvas.width;
            blank.height = canvas.height;

            return canvas.toDataURL() == blank.toDataURL();
        }

        $scope.text = {
            titulo_screen_firma: utils._('TITULO_SCREEN_FIRMA'),
            firmar: utils._('FIRMAR')
        };

        $scope.firmar = function () {
            if (isCanvasBlank(canvas_firm)) {
                Notification.error('No puedes dejar la firma en blanco');
                return;
            };

            var firma = signaturePad.toDataURL();
            localStorageService.set('firma', firma);
            if (DEBUG) console.log(firma);
            var codigo = localStorageService.get('codigo');
            fsm.get_machine(codigo).then(function (machine) {
                machine.finalize();
                localStorageService.set('state', machine.state);
                $state.go(machine.state, {}, { reload: true });
            });

        }

    }]);