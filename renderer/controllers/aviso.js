app.controller('AvisoCtrl', [
    '$scope',
    'localStorageService',
    '$state',
    '$timeout',
    'utils',
    'fsm',
    'CONFIG',
    function ($scope, localStorageService, $state, $timeout, utils, fsm, CONFIG) {

        $scope.btn_aspecto = CONFIG.pantallas[CONFIG.pantalla].btn_aspecto;
        var t;
        var machine;
        var codigo = localStorageService.get('codigo');
        fsm.get_machine(codigo).then(function (m) {

            machine = m;

            $scope.text = {
                titulo_screen: utils._('TITULO_SCREEN_' + machine.state),
                finalizar: utils._('FINALIZAR')
            };
            
            t = $timeout(function () {
                machine.timeout();
                localStorageService.set('state', machine.state);
                $state.go(machine.state, {}, { reload: true });
            }, CONFIG['timeout_to_alarm']);

            $scope.finalizar = function () {
                $timeout.cancel(t);
                machine.confirm();
                localStorageService.set('state', machine.state);
                $state.go(machine.state, {}, { reload: true });
            }
        });

    }]);