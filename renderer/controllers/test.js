app.controller('TestCtrl', [
    '$scope',
    'request',
    'db',
    'utils',
    'CONFIG',
    function ($scope, request, db, utils, CONFIG) {

        // request.get_paquetes();

        // var estado =
        //     {
        //         "estados": [
        //             {
        //                 "Partner": "2",
        //                 "id": "3",
        //                 "CodigoServicio": "ValorCodigoServicio",
        //                 "FechaHora": utils.datetime(),
        //                 "Estado": "2",
        //                 "NIF": "ValorNIF",
        //                 "Firma": "ValorFirma"
        //             }
        //         ]
        //     };
        // request.post_estado(estado);

        // console.log(utils.datetime());

        paquetes = request.get_paquetes();
        
        paquetes.then(
            function (response) {
                console.log(response);
                response.forEach(function(paquete){
                    db.insert_or_update_paquete(paquete);
                });
            },
            function (response) {
                console.log('error al recuperar los paquetes desde el webservice')
            }
        );

    }]);