app.controller('EndCtrl', [
    '$scope',
    'localStorageService',
    '$state',
    'utils',
    'fsm',
    function ($scope, localStorageService, $state, utils, fsm) {

        $scope.text = {
            titulo_screen_end: utils._('TITULO_SCREEN_END'),
            finalizar: utils._('Finalizar')
        };

        var codigo = localStorageService.get('codigo');
        fsm.get_machine(codigo).then(function (machine) {

            $scope.finalizar = function () {
                machine.restart();
                localStorageService.set('state', machine.state);
                $state.go(machine.state, {}, { reload: true });
            }

        });



    }]);