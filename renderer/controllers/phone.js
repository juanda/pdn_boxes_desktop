app.controller('PhoneCtrl', [
    '$scope',
    'localStorageService',
    '$state',
    'Notification',
    'utils',
    'fsm',
    'CONFIG',
    function ($scope, localStorageService, $state, Notification, utils, fsm, CONFIG) {
        
        $scope.css_keyboard = './styles/jkeyboard.' + CONFIG.pantalla + '.css';
        $scope.input_aspecto = CONFIG.pantallas[CONFIG.pantalla].input_aspecto;
        
        $scope.text = {
            'intro_phone': utils._('INTRO_PHONE'),
            'bad_phone': utils._('BAD_PHONE') + ' ' + localStorageService.get('tries_phone')
        };

        $('#phone').focus();

        $('#keyboard').jkeyboard({
            layout: "numeric_p360",
            input: $('#phone'),
        });

        // Cuando se pulsa la tecla 'OK'
        $('.return').click(function () {
            var phone = $('#phone').val().toUpperCase();
            var codigo = localStorageService.get('codigo');
            fsm.get_machine(codigo).then(function (machine) {
                machine.askForDoc(phone);
                localStorageService.set('state', machine.state);
                if (machine.state === "phone" && parseInt(localStorageService.get('tries_phone')) > 0) {
                    Notification.error(utils._('BAD_PHONE') + ' ' + localStorageService.get('tries_phone'));
                } else {
                    $state.go(machine.state, {}, { reload: true });
                }
            });

        });

    }]);