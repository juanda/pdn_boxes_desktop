const { dialog } = require('electron').remote;

app.controller('InicioCtrl', [
    '$scope',
    '$http',
    '$state',
    '$window',
    'localStorageService',
    'utils',
    'validator',
    'fsm',
    'db',
    '$http',
    'CONFIG',
    function ($scope, $http, $state, $window, localStorageService, utils, validator, fsm, db, $http, CONFIG) {
        
        if(CONFIG['test']){
            $state.go('test');    
        }

        $('#codigo').focus();

        $scope.bandera_altura = CONFIG.pantallas[CONFIG.pantalla].bandera_altura;
        $scope.sep_bandera = CONFIG.pantallas[CONFIG.pantalla].sep_bandera;
        $scope.img_scanner_altura = CONFIG.pantallas[CONFIG.pantalla].img_scanner_altura;
        $scope.pantalla = CONFIG.pantalla;
        $scope.input_aspecto = CONFIG.pantallas[CONFIG.pantalla].input_aspecto;
        $scope.css_keyboard = './styles/jkeyboard.' + CONFIG.pantalla + '.css';
        
        $scope.banderas = CONFIG.banderas;
        
        localStorageService.set('tries_nif', 0);
        localStorageService.set('tries_phone', 0);
        localStorageService.set('tries_doc', 0);

        $scope.text = {
            aproxime_codigo_lector: utils._('APROXIME_CODIGO_LECTOR'),
            con_codigo: utils._('CON_CODIGO'),
            con_clave: utils._('CON_CLAVE'),
            cambio_idioma: utils._('CAMBIO_IDIOMA')
        }

        $scope.change_language = function (lang) {
            console.log(lang);
            localStorageService.set("lang", lang);
            $state.go($state.current, {}, { reload: true });
        };

        // https://github.com/javidan/jkeyboard
        $('#keyboard').jkeyboard({
            layout: "numeric_p360",
            input: $('#codigo'),
        });

        function go(){
            var codigo = $('#codigo').val();
            localStorageService.set('codigo', codigo);
            localStorageService.set('state', 'inicio');
            fsm.get_machine(codigo).then(function (machine) {
                machine.confirmCodigo(codigo);
                localStorageService.set('state', machine.state);
                $state.go(machine.state, {}, { reload: true });
            });
        }

        // Cuando se pulsa enter
        $scope.intro = function(e){
            if(e.keyCode == 13){
                go();
            }
        };

        // Cuando se pulsa la tecla 'OK'
        $('.return').click(function () {
           go();
        });

    }]);
