app.controller('AlarmCtrl', [
    '$scope',
    'localStorageService',
    '$state',
    '$timeout',
    'utils',
    'fsm',
    function ($scope, localStorageService, $state, $timeout, utils, fsm) {

        $scope.text = {
            titulo_screen_alarm: utils._('TITULO_SCREEN_ALARM'),
            finalizar: utils._('FINALIZAR')
        };


        $scope.finalizar = function () {

            var codigo = localStorageService.get('codigo');
            fsm.get_machine(codigo).then(function (machine) {
                machine.confirm();
                localStorageService.set('state', machine.state);
                $state.go(machine.state, {}, { reload: true });

            });

        }

    }]);