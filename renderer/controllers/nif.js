app.controller('NifCtrl', [
    '$scope',
    'localStorageService',
    '$state',
    'Notification',
    'utils',
    'fsm',
    'db',
    'CONFIG',
    function ($scope, localStorageService, $state, Notification, utils, fsm, db, CONFIG) {
    
        $scope.css_keyboard = './styles/jkeyboard.' + CONFIG.pantalla + '.css';
        $scope.input_aspecto = CONFIG.pantallas[CONFIG.pantalla].input_aspecto;

        $scope.text = {
            'intro_nif': utils._('INTRO_NIF'),
            'bad_nif': utils._('BAD_NIF') + ' ' + localStorageService.get('tries_nif')
        };

        $('#nif').focus();

        $('#keyboard').jkeyboard({
            layout: "alpha_p360",
            input: $('#nif'),
        });

        // Cuando se pulsa la tecla 'OK'
        $('.return').click(function () {
            var nif = $('#nif').val().toUpperCase();
            var codigo = localStorageService.get('codigo');
            fsm.get_machine(codigo).then(function (machine) {
                machine.sign(nif);
                localStorageService.set('state', machine.state);
                if (machine.state === "nif" && parseInt(localStorageService.get('tries_nif')) > 0) {
                    Notification.error(utils._('BAD_NIF') + ' ' + localStorageService.get('tries_nif'));
                } else {
                    $state.go(machine.state, {}, { reload: true });
                }
            });

        });
    }]);