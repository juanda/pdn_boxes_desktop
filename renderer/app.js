var app = angular.module('app', [
    'ui.router',
    'ds.clock',
    'LocalStorageModule',
    'ui-notification',
    'angularSpinner'
]);
app.config([
    '$stateProvider',
    '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('menu', {
                url: '/menu',
                templateUrl: `${__dirname}/views/menu.html`,
                controller: 'MenuCtrl',
                controllerAs: 'menu_ctrl'
            })
            .state('inicio', {
                url: '/',
                templateUrl: `${__dirname}/views/inicio.html`,
                controller: 'InicioCtrl',
                controllerAs: 'inicio_ctrl'
            })
            .state('error_code', {
                url: '/error_code',
                templateUrl: `${__dirname}/views/error.html`,
                controller: 'ErrorCtrl',
                controllerAs: 'error_ctrl'
            })
            .state('phone', {
                url: '/phone',
                templateUrl: `${__dirname}/views/phone.html`,
                controller: 'PhoneCtrl',
                controllerAs: 'phone_ctrl'
            })
            .state('error_phone', {
                url: '/error_phone',
                templateUrl: `${__dirname}/views/error.html`,
                controller: 'ErrorCtrl',
                controllerAs: 'error_ctrl'
            })
            .state('nif', {
                url: '/nif',
                templateUrl: `${__dirname}/views/nif.html`,
                controller: 'NifCtrl',
                controllerAs: 'nif_ctrl'
            })
            .state('error_nif', {
                url: '/error_nif',
                templateUrl: `${__dirname}/views/error.html`,
                controller: 'ErrorCtrl',
                controllerAs: 'error_ctrl'
            })
            .state('doc', {
                url: '/doc',
                templateUrl: `${__dirname}/views/doc.html`,
                controller: 'DocCtrl',
                controllerAs: 'doc_ctrl'
            })
            .state('error_doc', {
                url: '/error_doc',
                templateUrl: `${__dirname}/views/error.html`,
                controller: 'ErrorCtrl',
                controllerAs: 'error_ctrl'
            })
            .state('firm', {
                url: '/firm',
                templateUrl: `${__dirname}/views/firm.html`,
                controller: 'FirmCtrl',
                controllerAs: 'firm_ctrl'
            })
            .state('end', {
                url: '/end',
                templateUrl: `${__dirname}/views/aviso.html`,
                controller: 'AvisoCtrl',
                controllerAs: 'aviso_ctrl'
            })
            .state('remove_package', {
                url: '/remove_package',
                templateUrl: `${__dirname}/views/aviso.html`,
                controller: 'AvisoCtrl',
                controllerAs: 'aviso_ctrl'
            })
            .state('doors_opened', {
                url: '/doors_opened',
                templateUrl: `${__dirname}/views/aviso.html`,
                controller: 'AvisoCtrl',
                controllerAs: 'aviso_ctrl'
            })
            .state('package_removed', {
                url: '/package_removed',
                templateUrl: `${__dirname}/views/aviso.html`,
                controller: 'AvisoCtrl',
                controllerAs: 'aviso_ctrl'
            })
            .state('insert_package', {
                url: '/insert_package',
                templateUrl: `${__dirname}/views/aviso.html`,
                controller: 'AvisoCtrl',
                controllerAs: 'aviso_ctrl'
            })
            .state('package_inserted', {
                url: '/package_inserted',
                templateUrl: `${__dirname}/views/aviso.html`,
                controller: 'AvisoCtrl',
                controllerAs: 'aviso_ctrl'
            })
            .state('no_box', {
                url: '/no_box',
                templateUrl: `${__dirname}/views/aviso.html`,
                controller: 'AvisoCtrl',
                controllerAs: 'aviso_ctrl'
            })
            .state('alarm', {
                url: '/alarm',
                templateUrl: `${__dirname}/views/alarm.html`,
                controller: 'AlarmCtrl',
                controllerAs: 'alarm_ctrl'
            })
            .state('test', {
                url: '/test',
                templateUrl: `${__dirname}/views/test.html`,
                controller: 'TestCtrl',
                controllerAs: 'test_ctrl'
            })
            ;

    }]);

app.run([
    '$timeout',
    'request',
    'db',
    '$window',
    'CONFIG',
    function ($timeout, request, db, $window, CONFIG) {

        var request_paquetes_time = CONFIG['request_paquetes_time'];
        var sync_paquetes = function () {
            $timeout(function () {

                paquetes = request.get_paquetes();

                paquetes.then(
                    function (response) {
                        console.log(response);
                        response.forEach(function (paquete) {
                            db.insert_or_update_paquete(paquete);
                        });
                    },
                    function (response) {
                        console.log('error al recuperar los paquetes desde el webservice')
                    }
                );

                sync_paquetes();
            }, request_paquetes_time);
        };

        sync_paquetes();

    }
]);