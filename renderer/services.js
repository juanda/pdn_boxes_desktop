const fs = require('fs');
const path = require('path')
const StateMachine = require('javascript-state-machine');
const mysql = require('promise-mysql');
const snmp = require('snmp-native');

// Volcado de información a la consola
DEBUG = 0;
// Simula una sesión sin usar la base de datos, en esta sesión:
// Códigos de páquetes válidos:
// 1 -> recogica con nif 
// 2 -> recogida con teléfono
// 3 -> retirada
// 4 -> inserción
// el valores válido del nif, telefono y documeto es 112 para todos ellos
SIMULATE = 0;

app.service('boxes', [
    '$timeout',
    function ($timeout) {

        // https://github.com/calmh/node-snmp-native
        var session = new snmp.Session({
            port: 161,
            community: 'private'
        });

        function callback(error, varbind) {
            if (error && DEBUG) {
                console.log('Fail :(');
                console.log(error);
            } else if (DEBUG) {
                console.log('The snmp set is done.');
                console.log(d);
                console.log(varbind);
            }
        }

        var _relay_off = function (ip, number) {
            var d = {
                oid: [1, 3, 6, 1, 4, 1, 19865, 1, 2, 2, number, 0],
                value: 0,
                type: 2,
                host: ip
            };
            session.set(d, callback);
        };

        var _relay_on = function (ip, number) {
            var d = {
                oid: [1, 3, 6, 1, 4, 1, 19865, 1, 2, 2, number, 0],
                value: 1,
                type: 2,
                host: ip
            };

            session.set(d, callback);

        };

        var open_door = function (ip, number) {
            _relay_on(ip, number);
            $timeout(function () { _relay_off(ip, number) }, 500);
        };

        // Esta función recursiva es necesaria para que los timeouts se vayan
        // concatenando secuencialmente, es decir, que cada timeout se dispare 
        // cuando haya finalizado el anterior. Si no se hace así, se envían varias
        // peticiones snmp simultaneamente en la misma sesión y el cacharro se 
        // hace la picha un lío.
        var open_doors = function (cajones, index = 0) {
            if (DEBUG) console.log('enter boxes.open_doors with cajones and index:');
            if (DEBUG) console.log(cajones);
            if (DEBUG) console.log(index);
            open_door(cajones[index].ip, cajones[index].number);
            $timeout(function () {
                index = index + 1;
                if (index < cajones.length) {
                    open_doors(cajones, index);
                }
            }, 1000);
        };

        return {
            open_door: open_door,
            open_doors: open_doors
        };
    }
]);

app.service('request', [
    '$http',
    'CONFIG',
    '$window',
    function ($http, CONFIG, $window) {
        var get_paquetes = function () {
            var url = CONFIG['url_api'] + '/' + CONFIG['ws_paquetes'] + '?task=' + CONFIG['token_paquetes'];
            var p = $http.get(url)
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (response) {
                        if (DEBUG) $window.alert('No he podido conectar con el servicio web');
                        if (DEBUG) console.log(response);
                    });
            return p;
        };

        var post_estado = function (estado) {
            var url = CONFIG['url_api'] + '/' + CONFIG['ws_estados'] + '?task=' + CONFIG['token_estados'];
            var p = $http.post(url, estado)
                .then(
                    function (response) {
                        return response;
                    },
                    function (response) {
                        if (DEBUG) $window.alert('No he podido conectar con el servicio web');
                        if (DEBUG) console.log(response);
                    });

            return p;
        };

        return {
            get_paquetes: get_paquetes,
            post_estado: post_estado
        }
    }
]);

app.service('db', [
    '$window',
    'CONFIG',
    function ($window, CONFIG) {

        //https://github.com/mysqljs/mysql
        //https://github.com/lukeb-uk/node-promise-mysql

        var con = mysql.createConnection({
            host: CONFIG['database']['host'],
            user: CONFIG['database']['user'],
            password: CONFIG['database']['password'],
            database: CONFIG['database']['database']
        });

        var connection;

        var get_paquete_info = function (codigo) {

            var info_paquete = {};

            var p = con.then(function (conn) {
                connection = conn;
                // consultamos el paquete a insertar o entregar que coincide con el código
                var query = "SELECT p.idAlbaran, p.cod_servicio, p.id as pid, p.num_albaran, p.barcode as barcode, p.nombre as nombre, " +
                    "p.nif as nif, p.telefono as telefono, p.retirada as retirada, " +
                    "p.estado as estado, c.id as cid, c.ip as cajon_ip, " +
                    "c.numero as cajon_numero, c.vacio as cajon_vacio " +
                    "FROM paquetes as p, cajones as c, cajon_paquete as cp " +
                    "WHERE p.barcode = '" + codigo + "' AND " +
                    "p.estado in ('asignado', 'insertado') AND " +
                    "cp.id_paquete = p.id AND " +
                    "cp.id_cajon = c.id";
                if (DEBUG) console.log(query);
                var paquetes = conn.query(query);
                return paquetes;

            }).then(function (paquetes) {

                // Si no se han encontrado paquetes, puede ser que no tenga
                // cajón asociado, en cuyo caso hay que cambiar la query
                if (paquetes.length == 0) {
                    // consultamos el paquete a insertar o entregar que coincide con el código
                    var query = "SELECT p.id as pid, p.num_albaran, p.barcode as barcode, p.nombre as nombre, " +
                        "p.nif as nif, p.telefono as telefono, p.retirada as retirada, " +
                        "p.estado as estado " +
                        "FROM paquetes as p " +
                        "WHERE p.barcode = '" + codigo + "'";
                    if (DEBUG) console.log(query);
                    var paquetes = connection.query(query);
                }

                return paquetes;
            })
                .then(function (paquetes) {
                    // Ahora sacamos el paquete (la anterio query devuelve un array),
                    // o un false, en caso de que no haya paquete.
                    if (DEBUG) console.log(paquetes)
                    if (paquetes.length == 0) {
                        return false;
                    }

                    info_paquete = {
                        paquete: paquetes[0],
                    }

                    return paquetes[0];
                }).then(function (paquete) {

                    // Ahora vemos todos los paquetes que deberían retirarse
                    var query = "SELECT p.id as pid, p.num_albaran, p.barcode as barcode, p.nombre as nombre, " +
                        "p.nif as nif, p.telefono as telefono, p.retirada as retirada, " +
                        "p.estado as estado, c.id as cid, c.ip as cajon_ip, " +
                        "c.numero as cajon_numero, c.vacio as cajon_vacio " +
                        "FROM paquetes as p, cajones as c, cajon_paquete as cp " +
                        "WHERE " +
                        "p.estado = 'insertado' AND " +
                        "p.retirada = 1 AND " +
                        "cp.id_paquete = p.id AND " +
                        "cp.id_cajon = c.id";

                    if (DEBUG) console.log(query);
                    var paquetes = connection.query(query);

                    return paquetes;
                }).then(function (paquetes) {
                    // y los metemos en un array
                    return {
                        paquete: info_paquete.paquete,
                        paquetes_retirar: paquetes
                    }

                });
            return p;
        };

        var paquete_entregado = function (paquete) {
            if (SIMULATE) return;
            var connection;
            var p = con
                .then(function (conn) {
                    connection = conn;
                    var query = "UPDATE paquetes SET estado = 'entregado' WHERE id = " +
                        paquete.pid.toString();
                    if (DEBUG) console.log(query);
                    var result = conn.query(query);
                    return result;
                })
                .then(function (rows) {
                    var query = "UPDATE cajones SET vacio=1 WHERE id=" +
                        paquete.cid.toString();
                    if (DEBUG) console.log(query);
                    var result = connection.query(query);
                    return result;
                })
                // liberamos los cajones
                .then(function (rows) {
                    var query = "DELETE FROM cajon_paquete where id_paquete=" + paquete.cid.toString();
                    if (DEBUG) console.log(query);
                    var result = connection.query(query);
                    return result;
                })
                .then(function (rows) {
                    var query = "UPDATE cajones SET vacio = 1 where id=" + paquete.cid.toString();
                    if (DEBUG) console.log(query);
                    var result = connection.query(query);
                    return result;
                })
                ;

            return p;
        };

        var paquetes_retirados = function (paquetes) {
            if (SIMULATE) return;
            var connection;
            var ids_paquetes = [];
            var ids_cajones = [];
            paquetes.forEach(function (paq) {
                ids_paquetes.push(paq.pid);
                ids_cajones.push(paq.cid);
            });
            var p = con
                .then(function (conn) {
                    connection = conn;
                    var query = "UPDATE paquetes SET estado = 'retirado' WHERE paquetes.id in (" +
                        ids_paquetes.join() + ")";
                    if (DEBUG) console.log(query);
                    var result = conn.query(query);
                    return result;
                })
                .then(function (rows) {
                    var query = "UPDATE cajones SET vacio=1 WHERE cajones.id in (" +
                        ids_cajones.join() + ")";
                    if (DEBUG) console.log(query);
                    var result = connection.query(query);
                    return result;
                })
                .then(function (row) {
                    var query = "DELETE FROM cajon_paquete where id_paquete in (" +
                        ids_paquetes.join() + ")";
                    if (DEBUG) console.log(query);
                    var result = connection.query(query);
                    return result;
                })
                .then(function (rows) {
                    var query = "UPDATE cajones SET vacio = 1 where id in (" +
                        ids_cajones.join() + ")";
                    if (DEBUG) console.log(query);
                    var result = connection.query(query);
                    return result;
                })

            return p;
        };

        var paquete_insertado = function (paquete) {
            if (SIMULATE) return;
            var connection;
            var p = con
                .then(function (conn) {
                    connection = conn;
                    var query = "UPDATE paquetes SET estado = 'insertado' WHERE paquetes.id = " +
                        paquete.pid.toString();
                    if (DEBUG) console.log(query);
                    var result = conn.query(query);
                    return result;
                })
                .then(function (rows) {
                    var query = "UPDATE cajones SET vacio=0 WHERE cajones.id=" +
                        paquete.cid.toString() + ";";
                    if (DEBUG) console.log(query);
                    var result = connection.query(query);
                    return result;
                });
            return p;
        };

        var insert_or_update_paquete = function (paquete) {
            if (SIMULATE) return;

            var connection;
            var p = con
                .then(function (conn) {
                    connection = conn;
                    var query = `SELECT * FROM paquetes WHERE barcode='${paquete.barcode}'`;
                    if (DEBUG) console.log(query);
                    var result = connection.query(query);
                    return result;
                })
                .then(function (rows) {
                    var query;
                    if (DEBUG) console.log(paquete);
                    if (rows.length == 0) { // paquete nuevo: insertar
                        query = `INSERT INTO paquetes 
                        (barcode, num_albaran, nombre, nif, telefono,
                        retirada, estado, cod_servicio, idAlbaran)
                        VALUES ('${paquete.barcode}', '${paquete.num_albaran}', 
                        '${paquete.nombre}', '${paquete.nif}', '${paquete.telefono}',
                         '${paquete.Retirar}', 'asignado', '${paquete.cod_servicio}', '${paquete.id}')`;
                    } else {
                        paquete.id_in_table = rows[0].id;
                        query = `UPDATE paquetes SET num_albaran='${paquete.num_albaran}',
                         nombre='${paquete.nombre}', nif='${paquete.nif}', 
                         telefono='${paquete.telefono}',
                         retirada='${paquete.Retirar}', estado='asignado', cod_servicio='${paquete.cod_servicio}'
                         WHERE barcode='${paquete.barcode}'`;
                    }
                    if (DEBUG) console.log(query);
                    var result = connection.query(query);
                    return result;
                })
                .then(function (rows) {

                    if (paquete.id_in_table == undefined) {
                        paquete.id_in_table = rows.insertId;
                    }

                    var query;
                    if (paquete.NumeroCajon != null) {
                        query = `DELETE FROM cajon_paquete WHERE id_paquete = ${paquete.id_in_table}`;
                        if (DEBUG) console.log(query);
                        var result = connection.query(query);
                        return result;
                    }
                })
                .then(function (rows) {
                    var query;
                    if (paquete.NumeroCajon != null) {
                        query = `INSERT into cajon_paquete (id_cajon, id_paquete)
                        VALUES (${paquete.NumeroCajon}, ${paquete.id_in_table})`
                        if (DEBUG) console.log(query);
                        var result = connection.query(query);
                        return result;
                    }
                });

            return p;

        };

        var save_estado = function (estado) {
            if (SIMULATE) return;

            var connection;
            var p = con
                .then(function (conn) {
                    connection = conn;
                    var query = `INSERT INTO estados 
                        (idAlbaran, CodigoServicio, Estado, FechaHora, NIF, Firma)
                        VALUES ('${estado.idAlbaran}', '${estado.CodigoServicio}', 
                        '${estado.Estado}', '${estado.FechaHora}', '${estado.NIF}',
                         '${estado.Firma}')`;

                    if (DEBUG) console.log(query);
                    var result = connection.query(query);
                    return result;
                });
            return p;

        };

        var close_connection = function () {
            con.end();
        };

        return {
            get_paquete_info: get_paquete_info,
            paquete_entregado: paquete_entregado,
            paquete_insertado: paquete_insertado,
            paquetes_retirados: paquetes_retirados,
            insert_or_update_paquete: insert_or_update_paquete,
            save_estado: save_estado,
            close_connection: close_connection
        };

    }
]);

app.service('utils', [
    '$http',
    'localStorageService',
    'CONFIG',
    function ($http, localStorageService, CONFIG) {

        if (localStorageService.get("lang") == undefined) {
            localStorageService.set("lang", "es");
        }
        json_trans_obj = CONFIG['translations'];

        var get_text = function (text) {
            lang = localStorageService.get("lang");

            if (json_trans_obj[text] == undefined
                || json_trans_obj[text][lang] === undefined)
                return text;
            else
                return json_trans_obj[text][lang];
        }

        var datetime = function () {
            var d = new Date();
            var date = d.toISOString().slice(0, 10);
            var time = d.toTimeString().slice(0, 8);
            var dt = date + " " + time;

            return dt;
        }

        return {
            _: get_text,
            datetime: datetime
        }

    }]);

app.service('validator', [
    'localStorageService',
    function (localStorageService) {

        if (localStorageService.get("tries_nif") == undefined) {
            localStorageService.set("tries_nif", 0);
        }
        if (localStorageService.get("tries_phone") == undefined) {
            localStorageService.set("tries_phone", 0);
        }
        if (localStorageService.get("tries_doc") == undefined) {
            localStorageService.set("tries_doc", 0);
        }

        // paquete puede ser un registro de la tabla paquetes combinado con su
        // cajon o false
        var get_validator_func = function (paquete) {

            var inc_tries_nif = function () {
                t = parseInt(localStorageService.get('tries_nif')) + 1;
                localStorageService.set('tries_nif', t);
            };

            var reset_tries_nif = function () {
                localStorageService.set('tries_nif', 0);
            };

            var tries_nif = function () {
                return localStorageService.get('tries_nif')
            };

            var inc_tries_phone = function () {
                t = parseInt(localStorageService.get('tries_phone')) + 1;
                localStorageService.set('tries_phone', t);
            };

            var reset_tries_phone = function () {
                localStorageService.set('tries_phone', 0);
            };

            var tries_phone = function () {
                return localStorageService.get('tries_phone')
            };

            var inc_tries_doc = function () {
                t = parseInt(localStorageService.get('tries_doc')) + 1;
                localStorageService.set('tries_doc', t);
            };

            var reset_tries_doc = function () {
                localStorageService.set('tries_doc', 0);
            };

            var tries_doc = function () {
                return localStorageService.get('tries_doc')
            };


            var check_codigo,
                check_nif,
                has_nif,
                check_nif,
                check_phone,
                check_doc,
                has_packages_to_remove,
                package_to_insert;

            if (SIMULATE) {
                var check_codigo = function (codigo) {
                    if (['1', '2', '3', '4'].indexOf(codigo) !== -1) {
                        return true
                    }
                    return false;
                };

                var has_nif = function (codigo) {
                    if (codigo === '1') {
                        return true;
                    }
                    return false;
                };

                var check_nif = function (nif) {
                    if (nif === '112') {
                        return true;
                    }
                    return false;
                };

                var check_phone = function (phone) {
                    if (phone === '112') {
                        return true;
                    }
                    return false;
                };

                var check_doc = function (doc) {
                    if (doc === '112') {
                        return true;
                    }
                    return false;
                };

                var has_packages_to_remove = function (codigo) {
                    if (codigo === '3') {
                        return true;
                    }
                    return false;
                };

                var package_to_insert = function (codigo) {
                    if (['3', '4'].indexOf(codigo) !== -1) {
                        return true;
                    }
                    return false;
                }
            } else {
                console.log(paquete)
                var check_codigo = function (codigo) {
                    if (paquete) {
                        return paquete.barcode === codigo;
                    }
                    return false;
                };

                var has_nif = function (codigo) {
                    return paquete.nif !== null && paquete.nif != "";

                };

                var check_nif = function (nif) {
                    return nif === paquete.nif;

                };

                var check_phone = function (phone) {
                    return phone === paquete.telefono;

                };

                var check_doc = function (doc) {

                    return true;
                };

                var has_packages_to_remove = function (paquetes_retirar) {
                    return paquetes_retirar.length > 0;

                };

                var package_to_insert = function (paquete) {

                    return paquete.cajon_vacio;

                }

                var has_box = function () {
                    return (paquete.cajon_numero == undefined) ? false : true;
                }

                var entregado = function () {
                    if (paquete == undefined) {
                        return true
                    }else{
                        return paquete.estado == 'entregado'
                    }
                    
                }
            }


            return {
                inc_tries_nif: inc_tries_nif,
                reset_tries_nif: reset_tries_nif,
                tries_nif: tries_nif,
                inc_tries_phone: inc_tries_phone,
                reset_tries_phone: reset_tries_phone,
                tries_phone: tries_phone,
                inc_tries_doc: inc_tries_doc,
                reset_tries_doc: reset_tries_doc,
                tries_doc: tries_doc,
                check_codigo: check_codigo,
                has_nif: has_nif,
                check_nif: check_nif,
                check_phone: check_phone,
                check_doc: check_doc,
                has_packages_to_remove: has_packages_to_remove,
                package_to_insert: package_to_insert,
                has_box: has_box,
                entregado: entregado
            };
        };

        return {
            get_validator_func: get_validator_func
        }


    }]);

app.service('fsm', [
    'localStorageService',
    'validator',
    '$timeout',
    'boxes',
    'db',
    'request',
    'utils',
    'CONFIG',
    function (localStorageService, _validator, $timeout, boxes, db, request, utils, CONFIG) {

        var get_machine = function (codigo) {

            var promesa = db.get_paquete_info(codigo)
                .then(function (info_paquete) {
                    // paquete puede ser un registro de la tabla paquetes combinado con su
                    // cajon o false

                    var paquete = info_paquete.paquete;
                    var paquetes_retirar = info_paquete.paquetes_retirar;

                    var validator = _validator.get_validator_func(paquete);

                    var machine = StateMachine({
                        init: localStorageService.get('state'),
                        transitions: [
                            {
                                name: 'confirm_codigo',
                                from: 'inicio',
                                to: function (codigo) {
                                    if (SIMULATE) {
                                        paquete = codigo;
                                        paquetes_retirar = codigo;
                                    }
                                    if (validator.entregado()) {
                                        return 'error_code';
                                    }
                                    if (validator.check_codigo(codigo) && !validator.has_box()) {
                                        return 'no_box';
                                    }
                                    if (validator.check_codigo(codigo) && validator.package_to_insert(paquete)
                                        && validator.has_packages_to_remove(paquetes_retirar)) {
                                        return 'remove_package';
                                    }
                                    if (validator.check_codigo(codigo) && validator.package_to_insert(paquete)) {
                                        return 'insert_package';
                                    }
                                    if (validator.check_codigo(codigo) && validator.has_nif(codigo)) {
                                        return 'nif';
                                    }
                                    if (validator.check_codigo(codigo)) {
                                        return 'phone';
                                    }
                                    return 'error_code';
                                }
                            },
                            {
                                name: 'sign',
                                from: 'nif',
                                to: function (nif) {
                                    if (!validator.check_nif(nif) && validator.tries_nif() < 2) {
                                        validator.inc_tries_nif();
                                        return 'nif';
                                    }
                                    if (!validator.check_nif(nif) && validator.tries_nif() == 2) {
                                        validator.reset_tries_nif();
                                        return 'error_nif';
                                    }
                                    return 'firm';

                                }
                            },
                            {
                                name: 'sign',
                                from: 'doc',
                                to: function (doc) {
                                    if (!validator.check_doc(doc) && validator.tries_doc() < 2) {
                                        validator.inc_tries_doc();
                                        return 'doc';
                                    }
                                    if (!validator.check_doc(doc) && validator.tries_doc() == 2) {
                                        validator.reset_tries_doc();
                                        return 'error_doc';
                                    }
                                    return 'firm';
                                }
                            },
                            {
                                name: 'ask_for_doc',
                                from: 'phone',
                                to: function (phone) {
                                    if (!validator.check_phone(phone) && validator.tries_phone() < 2) {
                                        validator.inc_tries_phone();
                                        return 'phone';
                                    }
                                    if (!validator.check_phone(phone) && validator.tries_phone() == 2) {
                                        validator.reset_tries_phone();
                                        return 'error_phone';
                                    }
                                    return 'doc';
                                }
                            },
                            {
                                name: 'confirm',
                                from: 'remove_package',
                                to: 'doors_opened'
                            },
                            {
                                name: 'timeout',
                                from: 'remove_package',
                                to: 'alarm'
                            },
                            {
                                name: 'confirm',
                                from: 'doors_opened',
                                to: /*'package_removed'*/ 'insert_package'
                            },
                            {
                                name: 'timeout',
                                from: 'doors_opened',
                                to: 'alarm'
                            },
                            // {
                            //     name: 'confirm',
                            //     from: 'package_removed',
                            //     to: 'insert_package'
                            // },
                            // {
                            //     name: 'timeout',
                            //     from: 'package_removed',
                            //     to: 'alarm'
                            // },
                            {
                                name: 'confirm',
                                from: 'insert_package',
                                to: 'package_inserted'
                            },
                            {
                                name: 'timeout',
                                from: 'insert_package',
                                to: 'alarm'
                            },
                            {
                                name: 'confirm',
                                from: 'package_inserted',
                                to: 'inicio'
                            },
                            {
                                name: 'timeout',
                                from: 'package_inserted',
                                to: 'alarm'
                            },
                            {
                                name: 'confirm',
                                from: 'alarm',
                                to: 'inicio'
                            },
                            {
                                name: 'finalize',
                                from: 'firm',
                                to: 'end'
                            },
                            {
                                name: 'confirm',
                                from: 'end',
                                to: 'inicio'
                            },
                            {
                                name: 'confirm',
                                from: 'no_box',
                                to: 'inicio'
                            },
                            {
                                name: 'abort',
                                from: ['error_nif', 'error_phone', 'error_doc', 'error_code'],
                                to: 'inicio'
                            },
                            {
                                name: 'timeout',
                                from: ['error_nif', 'error_phone', 'error_doc', 'error_code'],
                                to: 'inicio'
                            },
                            {
                                name: 'timeout',
                                from: 'error_code',
                                to: 'inicio'
                            },

                        ],
                        methods: {}
                    });

                    var open_doors = function () {
                        /// ESTO HAY QUE BORRARLO
                        // var _cajones = [
                        //     {
                        //         ip: '172.16.100.2',
                        //         number: 1
                        //     },
                        //     {
                        //         ip: '172.16.100.2',
                        //         number: 4
                        //     },
                        //     {
                        //         ip: '172.16.100.2',
                        //         number: 8
                        //     }
                        // ];
                        // var _cajones2 = [
                        //     {
                        //         ip: '172.16.100.2',
                        //         number: 5
                        //     }
                        // ];
                        // localStorageService.set('cajones', _cajones);
                        // HASTA AQUI

                        var cajones = localStorageService.get('cajones')

                        if (DEBUG) console.log('Abriendo cajones ' + JSON.stringify(cajones));

                        boxes.open_doors(cajones);

                        localStorageService.remove('cajones');
                    };

                    machine.observe({
                        'onLeaveFirm': function () {
                            if (SIMULATE) return;
                            db.paquete_entregado(paquete)
                                .then(function () {
                                    var cajones = [{
                                        ip: paquete.cajon_ip,
                                        number: paquete.cajon_numero
                                    }];
                                    localStorageService.set('cajones', cajones);
                                    open_doors();
                                    return cajones;
                                })
                                .then(function (cajones) {
                                    var estado = {
                                        // no están en servicio juan //
                                        idAlbaran: paquete.idAlbaran,
                                        CodigoServicio: paquete.cod_servicio,
                                        FechaHora: utils.datetime(),
                                        Estado: CONFIG["estados"]["entregado"],
                                        NIF: paquete.nif,
                                        Firma: localStorageService.get('firma')
                                    };
                                    if (DEBUG) console.log(paquete);
                                    if (DEBUG) console.log(estado);
                                    // request.post_estado(estado).then(function (resp) {
                                    //    if (DEBUG) console.log(resp);
                                    // });
                                    db.save_estado(estado).then(function (resp) {
                                        if (DEBUG) console.log(resp);
                                    })
                                });

                        },
                        'onLeaveRemovePackage': function () {
                            if (SIMULATE) return;
                            // Esto es necesario por que despues de dejar este estado
                            // los paqutes retirados serán desasociados de los cajones
                            // y la próxima vez que se construya la máquina de estados
                            // el array paquetes_retirar estará vacío.
                            localStorageService.set('paquetes_retirados', paquetes_retirar);
                            db.paquetes_retirados(paquetes_retirar)
                                .then(function () {
                                    var cajones = [];
                                    paquetes_retirar.forEach(function (paq) {
                                        cajones.push({
                                            ip: paq.cajon_ip,
                                            number: paq.cajon_numero
                                        });
                                    });
                                    localStorageService.set('cajones', cajones);
                                    open_doors();
                                    return cajones;
                                })
                        },
                        'onLeaveInsertPackage': function () {
                            if (SIMULATE) return;
                            db.paquete_insertado(paquete)
                                .then(function () {
                                    var cajones = [{
                                        ip: paquete.cajon_ip,
                                        number: paquete.cajon_numero
                                    }];
                                    localStorageService.set('cajones', cajones);
                                    open_doors();
                                    return cajones;
                                })
                        },
                        'onLeavePackageInserted': function () {
                            var estado = {
                                idAlbaran: paquete.idAlbaran,
                                CodigoServicio: paquete.cod_servicio,
                                FechaHora: utils.datetime(),
                                Estado: CONFIG["estados"]["insertado"],
                                NIF: paquete.nif,
                                Firma: ""
                            };
                            if (DEBUG) console.log(paquete);
                            if (DEBUG) console.log(estado);
                            // request.post_estado(estado).then(function (resp) {
                            //     if (DEBUG) console.log(resp);
                            // });
                            db.save_estado(estado).then(function (resp) {
                                if (DEBUG) console.log(resp);
                            })
                        },
                        'onLeaveDoorsOpened': function () {

                            var send_state = function (paquetes, index = 0) {
                                var estado = {
                                    idAlbaran: paquetes[index].idAlbaran,
                                    CodigoServicio: paquetes[index].cod_servicio,
                                    FechaHora: utils.datetime(),
                                    Estado: CONFIG["estados"]["retirado"],
                                    NIF: paquetes[index].nif,
                                    Firma: ""
                                };
                                if (DEBUG) console.log(paquetes[index]);
                                if (DEBUG) console.log(estado);
                                // request.post_estado(estado).then(function (resp) {
                                //     if (DEBUG) console.log(resp);
                                // });
                                db.save_estado(estado).then(function (resp) {
                                    if (DEBUG) console.log(resp);
                                })

                                $timeout(function () {
                                    index = index + 1;
                                    if (index < paquetes.length) {
                                        send_state(paquetes, index);
                                    }
                                }, 200);
                            };

                            var paquetes_retirados = localStorageService.get('paquetes_retirados');
                            send_state(paquetes_retirados);
                        },
                        'onEnterInicio': function () {
                            var lang = localStorageService.get("lang");
                            localStorageService.clearAll();
                            localStorageService.set("lang", lang);
                        }
                    });

                    return machine;
                });
            return promesa;
        };

        return {
            get_machine: get_machine
        };

    }
]);
