const fs = require('fs');   

exports.get_json_config_obj = function(){
    var json_config_file;
    
    if (!process.env.PDN_BOXES_CONFIG) {
        process.env.PDN_BOXES_CONFIG = '/etc/pdn-boxes-desktop/config.json'
    }
    
    try {
        json_config_file = process.env.PDN_BOXES_CONFIG;
        json_config = fs.readFileSync(json_config_file, 'utf-8');
    }
    catch (e) {        
        
        return false;
    }
    
    json_config_obj = JSON.parse(json_config);

    return json_config_obj;
};