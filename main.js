const electron = require('electron');
// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;

const config = require('./config.js');

const path = require('path');
const url = require('url');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

var json_config_obj = config.get_json_config_obj();
if(!json_config_obj){
  console.log('No puedo encontrar el fichero de configuración. ' +
  'Crea uno en /etc/pdn-boxes-desktop/config.json o crea una ' +
  'variable de entorno denomindada PDN_BOXES_CONFIG con la ' +
  'ruta al fichero de configuración');
  process.exit();
}

global.sharedObj = json_config_obj;

function createWindow () {
  // Create the browser window.
  var cw = {
    width: parseInt(json_config_obj.pantallas[json_config_obj.pantalla].width), 
    height: parseInt(json_config_obj.pantallas[json_config_obj.pantalla].height),
    frame: false,
    icon:'./llave.png',
  };
  mainWindow = new BrowserWindow(cw);

  mainWindow.setFullScreen(true);;
  
  // and load the index.html of the app.
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, '/renderer/index.html'),
    protocol: 'file:',
    slashes: true
  }));

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
});

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
