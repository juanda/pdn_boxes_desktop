# PDN BOXES DESKTOP

This is an Electron application based on the [Quick Start Guide](http://electron.atom.io/docs/tutorial/quick-start) within the Electron documentation.

A basic Electron application needs just these files:

- `package.json` - Points to the app's main file and lists its details and dependencies.
- `main.js` - Starts the app and creates a browser window to render HTML. This is the app's **main process**.
- `index.html` - A web page to render. This is the app's **renderer process**.

You can learn more about each of these components within the [Quick Start Guide](http://electron.atom.io/docs/tutorial/quick-start).

I've started from such minimal application and tuned to work with angular.

## To Use

```bash
# Clone this repository
git clone https://bitbucket.com/juanda/pdn_boxes_desktop.git
# Go into the repository
cd pdn_boxes_desktop
# Install dependencies for main process
npm install
# Install dependencies for renderer rprocess
cd renderer
bower install

# Run the app
cd ..
npm start
```

The management of the main process is made with npm (package.json), while
the renderer process is managed with bower (renderer/bower.json). This way 
I can work as I use to do when developping web apps.

The application needs a mysql database. The management and control version is
made with python alembic. You can find it in ``sql`` directory.

To create the database you can use the dump file ``sql/pdn_boxes.sql``, or use alembic:

    # alembic upgrade head

See the following tutorial to learn how to make migrations:

    http://alembic.zzzcomputing.com/en/latest/tutorial.html


## Build a package for distribution

In the raspberry pi install:

    npm install -g electron-packager
    npm install -g electron-installer-debian

Then build the debian package:

    electron-packager . pdn-boxes-desktop --platform linux --arch armv7l --out dist/

This command can be launched from ``npm``:

    npm build

This creates a package which can be put in whatever directory and executed from a bash terminal
like this:

    /path/to/package/pdn_boxes_desktop

Next we can build a debian package as follow:

    electron-installer-debian --src dist/pdn-boxes-desktop-linux-armv7l/ --dest dist/installers/ --arch armhf

Also this command can be launched from ``npm`` like this:

    npm debian

The aplication uses as config file the one defined in the enviroment variable PDN_BOXES_CONFIG.
So you can add such enviroment variable in the .bashrc file. For instance:

    export PDN_BOXES_CONFIG=/home/pi/Apps/pdn_boxes_desktop/config.json

If the variable doesn't exist it is create automatically with the value:
    /etc/pdn-boxes-desktop/config.json

The config file is a JSON file with the following structure:

    {
        "timeout_to_alarm": 5000,
        "database": {
            "host": "127.0.0.1",
            "user": "root",
            "password": "root",
            "database": "pdn_boxes"
        }
    }

To install:

    sudo apt-get install libgconf2-4
    sudo dpkg -i dist/installers/electron-jd_1.0.0_armhf.deb

``timeout_to_alarm`` set the time to fire the alarm window when user don't press the button
to confirm the operation and go to the next step.



## Resources for Learning Electron

- [electron.atom.io/docs](http://electron.atom.io/docs) - all of Electron's documentation
- [electron.atom.io/community/#boilerplates](http://electron.atom.io/community/#boilerplates) - sample starter apps created by the community
- [electron/electron-quick-start](https://github.com/electron/electron-quick-start) - a very basic starter Electron app
- [electron/simple-samples](https://github.com/electron/simple-samples) - small applications with ideas for taking them further
- [electron/electron-api-demos](https://github.com/electron/electron-api-demos) - an Electron app that teaches you how to use Electron
- [hokein/electron-sample-apps](https://github.com/hokein/electron-sample-apps) - small demo apps for the various Electron APIs

## License

[CC0 1.0 (Public Domain)](LICENSE.md)
