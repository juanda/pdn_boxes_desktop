from setuptools import setup

setup(
    name='pdn_boxes_sql',
    packages=['pdn_boxes_sql'],
    version='0.1.0',
    author='Juan David Rodríguez García',
    description='Mantenimiento de la base de datos de pdn_boxes_desktop',
    license='MIT',
    author_email='juanda@juandarodriguez.es',
    include_package_data=True,
    install_requires=[
        'mysqlclient==1.3.12',
        'SQLAlchemy==1.1.15',
        'alembic==0.9.6',
        'pylint==1.8.1'
    ]
)
