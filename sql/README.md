# Mantenimiento de la base de datos pdn_boxes_desktop

Crear un virtualenv de python3

    mkvirtualenv pdn_boxes -p /usr/bin/python3

Instalar las dependencias

    pip install .

Ejecutar las migraciones

    alembic upgrade head

Para generar una nueva migración

    alembic revision
