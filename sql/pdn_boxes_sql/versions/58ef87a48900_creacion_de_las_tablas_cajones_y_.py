"""creacion de las tablas cajones y paquetes

Revision ID: 58ef87a48900
Revises: 
Create Date: 2018-01-09 13:13:55.676697+01:00

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '58ef87a48900'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'cajones',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('ip', sa.String(16), nullable=False),
        sa.Column('numero', sa.Integer),
        sa.Column('vacio', sa.Boolean, default=1, nullable=False)
    )

    op.create_table(
        'paquetes',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('barcode', sa.String(255), nullable=False, unique=True),
        sa.Column('num_albaran', sa.String(255)),
        sa.Column('nombre', sa.String(255)),
        sa.Column('vacio', sa.Boolean(), default=1, nullable=False)
    )

    op.create_table(
        'cajon_paquete',
        sa.Column('id_cajon', sa.Integer, sa.ForeignKey('cajones.id')),
        sa.Column('id_paquete', sa.Integer, sa.ForeignKey('paquetes.id')),
    )


def downgrade():
    op.drop_table('cajon_paquete')
    op.drop_table('paquetes')
    op.drop_table('cajones')
