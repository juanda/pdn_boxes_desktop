"""idAlbaran

Revision ID: 624945157792
Revises: 04cda4ec3bc3
Create Date: 2018-05-17 12:05:37.452221+02:00

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '624945157792'
down_revision = '04cda4ec3bc3'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('paquetes', sa.Column('idAlbaran', sa.Integer))


def downgrade():
    op.drop_column('paquetes', 'idAlbaran')
