"""tabla estados

Revision ID: 49582d47c03a
Revises: 624945157792
Create Date: 2018-05-17 12:09:04.215139+02:00

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '49582d47c03a'
down_revision = '624945157792'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'estados',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('idAlbaran', sa.Integer),
        sa.Column('CodigoServicio', sa.String(255)),
        sa.Column('Estado', sa.Integer, nullable=False),
        sa.Column('FechaHora', sa.DateTime, nullable=False),
        sa.Column('NIF', sa.String(255) ),
        sa.Column('Firma', sa.Text()),
        sa.Column('Enviado', sa.Boolean, default=False)
    )


def downgrade():
    op.drop_table('estados')
