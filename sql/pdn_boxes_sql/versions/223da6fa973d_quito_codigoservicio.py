"""quito codigoservicio

Revision ID: 223da6fa973d
Revises: 49582d47c03a
Create Date: 2018-05-17 13:04:00.530048+02:00

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '223da6fa973d'
down_revision = '49582d47c03a'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_column('paquetes', 'codigoservicio')


def downgrade():
    op.add_column('paquetes', sa.Column('codigoservicio', sa.String(255)))
