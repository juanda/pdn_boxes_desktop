"""modificacion tabla paquetes

Revision ID: 4d192c2df1c2
Revises: 58ef87a48900
Create Date: 2018-01-09 13:34:25.358401+01:00

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4d192c2df1c2'
down_revision = '58ef87a48900'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_column('paquetes', 'vacio')
    op.add_column('paquetes', sa.Column('nif', sa.String(16)))
    op.add_column('paquetes', sa.Column('telefono', sa.String(16)))
    op.add_column('paquetes', sa.Column('retirada', sa.Boolean))
    op.add_column('paquetes', sa.Column('estado', sa.String(16)))


def downgrade():
    op.add_column('paquetes', sa.Column('vacio', sa.Boolean))
    op.drop_column('paquetes', 'nif')
    op.drop_column('paquetes', 'telefono')
    op.drop_column('paquetes', 'retirada')
    op.drop_column('paquetes', 'estado')
