"""añado campo CodigoServicio

Revision ID: 04cda4ec3bc3
Revises: 4d192c2df1c2
Create Date: 2018-04-04 21:29:23.689733+02:00

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '04cda4ec3bc3'
down_revision = '4d192c2df1c2'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('paquetes', sa.Column('cod_servicio', sa.String(256)))


def downgrade():
    op.drop_column('paquetes', 'cod_servicio')
