-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 17, 2018 at 01:12 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.1.15-1+ubuntu16.04.1+deb.sury.org+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pdn_boxes`
--

-- --------------------------------------------------------

--
-- Table structure for table `alembic_version`
--

CREATE TABLE `alembic_version` (
  `version_num` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alembic_version`
--

INSERT INTO `alembic_version` (`version_num`) VALUES
('223da6fa973d');

-- --------------------------------------------------------

--
-- Table structure for table `cajones`
--

CREATE TABLE `cajones` (
  `id` int(11) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `vacio` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cajones`
--

INSERT INTO `cajones` (`id`, `ip`, `numero`, `vacio`) VALUES
(201, '172.16.100.2', 1, 0),
(202, '172.16.100.2', 2, 1),
(203, '172.16.100.2', 3, 1),
(204, '172.16.100.2', 4, 1),
(205, '172.16.100.2', 5, 1),
(206, '172.16.100.2', 6, 1),
(207, '172.16.100.2', 7, 1),
(208, '172.16.100.2', 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cajon_paquete`
--

CREATE TABLE `cajon_paquete` (
  `id_cajon` int(11) DEFAULT NULL,
  `id_paquete` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cajon_paquete`
--

INSERT INTO `cajon_paquete` (`id_cajon`, `id_paquete`) VALUES
(201, 16),
(202, 17),
(203, 18),
(204, 19);

-- --------------------------------------------------------

--
-- Table structure for table `estados`
--

CREATE TABLE `estados` (
  `id` int(11) NOT NULL,
  `idAlbaran` int(11) DEFAULT NULL,
  `CodigoServicio` varchar(255) DEFAULT NULL,
  `Estado` int(11) NOT NULL,
  `FechaHora` datetime NOT NULL,
  `NIF` varchar(255) DEFAULT NULL,
  `Firma` text,
  `Enviado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paquetes`
--

CREATE TABLE `paquetes` (
  `id` int(11) NOT NULL,
  `barcode` varchar(255) NOT NULL,
  `num_albaran` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `nif` varchar(16) DEFAULT NULL,
  `telefono` varchar(16) DEFAULT NULL,
  `retirada` tinyint(1) DEFAULT NULL,
  `estado` varchar(16) DEFAULT NULL,
  `cod_servicio` varchar(256) DEFAULT NULL,
  `idAlbaran` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paquetes`
--

INSERT INTO `paquetes` (`id`, `barcode`, `num_albaran`, `nombre`, `nif`, `telefono`, `retirada`, `estado`, `cod_servicio`, `idAlbaran`) VALUES
(15, '411913136438670016', '313643867', 'JOSE LUIS', '', '670724934', 1, 'asignado', '41191313643867', 15),
(16, '411913136439040016', '313643904', 'JOSE LUIS', '', '670724934', 0, 'asignado', '41191313643904', 16),
(17, '411913136439360015', '313643936', 'JOSE LUIS', '', '670724934', 0, 'asignado', '41191313643936', 17),
(18, '411913136439690013', '313643969', 'JOSE LUIS', '', '670724934', 0, 'asignado', '41191313643969', 18),
(19, '411913136440200010', '313644020', 'JOSE LUIS', '', '670724934', 0, 'asignado', '41191313644020', 19);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alembic_version`
--
ALTER TABLE `alembic_version`
  ADD PRIMARY KEY (`version_num`);

--
-- Indexes for table `cajones`
--
ALTER TABLE `cajones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cajon_paquete`
--
ALTER TABLE `cajon_paquete`
  ADD KEY `id_cajon` (`id_cajon`),
  ADD KEY `id_paquete` (`id_paquete`);

--
-- Indexes for table `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paquetes`
--
ALTER TABLE `paquetes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `barcode` (`barcode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cajones`
--
ALTER TABLE `cajones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=209;
--
-- AUTO_INCREMENT for table `estados`
--
ALTER TABLE `estados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `paquetes`
--
ALTER TABLE `paquetes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cajon_paquete`
--
ALTER TABLE `cajon_paquete`
  ADD CONSTRAINT `cajon_paquete_ibfk_1` FOREIGN KEY (`id_cajon`) REFERENCES `cajones` (`id`),
  ADD CONSTRAINT `cajon_paquete_ibfk_2` FOREIGN KEY (`id_paquete`) REFERENCES `paquetes` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;